package lights.shader;

import com.badlogic.gdx.ApplicationAdapter;
import com.felixcool98.lights.shader.ShaderLightSystem;

import lights.test.LightSystemTest;

public class ShaderLightSystemTest extends ApplicationAdapter {
	public static void main(String[] args) {
		LightSystemTest.test(()->{return new ShaderLightSystem();});
	}
}
