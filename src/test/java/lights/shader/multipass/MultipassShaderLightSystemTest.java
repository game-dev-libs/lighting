package lights.shader.multipass;

import com.badlogic.gdx.ApplicationAdapter;
import com.felixcool98.lights.shader.multipass.MultipassShaderLightSystem;

import lights.test.LightSystemTest;

public class MultipassShaderLightSystemTest extends ApplicationAdapter {
	public static void main(String[] args) {
		LightSystemTest.test(()->{return new MultipassShaderLightSystem();});
	}
}
