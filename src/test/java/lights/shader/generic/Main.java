package lights.shader.generic;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.felixcool98.gdxutility.GdxUtils;
import com.felixcool98.lights.DirectionalLight;
import com.felixcool98.lights.PointLight;
import com.felixcool98.lights.generic.GenericLightSystem;
import com.felixcool98.lights.generic.LightDrawer;
import com.felixcool98.lights.generic.shadow.ShadowLightDrawer;
import com.felixcool98.lights.generic.simple.SimpleLightDrawer;

public class Main extends ApplicationAdapter {
	public static void main(String[] args) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		int width = 1280;
		int height = 720;
		
		config.setWindowSizeLimits(width, height, width, height);
		config.useOpenGL3(true, 3, 2);
		new Lwjgl3Application(new Main(), config);
	}
	
	
	private int lightDrawerIndex;
	private LightDrawer[] lightdrawers;
	
	
	private Texture game;
	private Texture normalMap;
	
	private OrthographicCamera camera;
	
	private SpriteBatch batch;
	
	private GenericLightSystem lights;
	
	private PointLight moving;
	
	
	@Override
	public void create() {
		game = new Texture(Gdx.files.local("test.png"));
		normalMap = new Texture(Gdx.files.local("test_normal.png"));
		
		camera = new OrthographicCamera(30, 30*GdxUtils.getAspectRatio());
		camera.position.set(camera.viewportWidth/2f, camera.viewportHeight/2f, 0);
		
		lightdrawers = new LightDrawer[] {
				new ShadowLightDrawer(),
				new SimpleLightDrawer()
		};
		
		lights = new GenericLightSystem(lightdrawers[lightDrawerIndex]);
		
		batch = new SpriteBatch();
		
		moving = new PointLight(0, 0, 8, Color.WHITE);
		
//		for(int i = 0; i < 100; i++)
//			lights.add(new PointLight(Random.INSTANCE.randomFloat(0, camera.viewportWidth), Random.INSTANCE.randomFloat(0, camera.viewportHeight), Random.INSTANCE.randomFloat(1, 3),
//					new Color(Random.INSTANCE.randomFloat(), Random.INSTANCE.randomFloat(), Random.INSTANCE.randomFloat(), 1)));
		
//		lights.add(new PointLight(0, 0, 3, Color.BLUE));
//		lights.add(new PointLight(0, 0, 3, Color.RED));
//		lights.add(new PointLight(2, 7, 1, Color.GREEN));
//		lights.add(new PointLight(10, 10, 4, Color.WHITE));
//		lights.add(new PointLight(12, 10, 4, Color.WHITE));//14 10 4 WHITE
		lights.add(new PointLight(0, 0, 10, Color.RED));//0 0 WHITE
		lights.add(new PointLight(6, 0, 10, Color.BLUE));
		lights.add(new DirectionalLight(Color.ORANGE, -1, 1));
//		lights.add(new PointLight(20, 10, 10, Color.WHITE));
//		lights.add(new PointLightSquare(3, 10, 1, 2, Color.CYAN));
//		lights.add(new PointLightSquare(4, 10, 1, 2, Color.CYAN));
		lights.add(moving);
	}
	
	@Override
	public void render() {
		if(Gdx.input.isKeyJustPressed(Keys.Q)) {
			lightDrawerIndex++;
			
			if(lightDrawerIndex >= lightdrawers.length)
				lightDrawerIndex = 0;
		}
		if(Gdx.input.isKeyJustPressed(Keys.E)) {
			lightDrawerIndex--;
			
			if(lightDrawerIndex < 0)
				lightDrawerIndex = lightdrawers.length-1;
		}
		
		lights.setLightDrawer(lightdrawers[lightDrawerIndex]);
		
		if(Gdx.input.isKeyPressed(Keys.W))
			camera.position.add(0, 0.1f, 0);
		if(Gdx.input.isKeyPressed(Keys.S))
			camera.position.add(0, -0.1f, 0);
		if(Gdx.input.isKeyPressed(Keys.A))
			camera.position.add(-0.1f, 0, 0);
		if(Gdx.input.isKeyPressed(Keys.D))
			camera.position.add(0.1f, 0, 0);
		
		if(Gdx.input.isKeyPressed(Keys.K))
			moving.setX(moving.getX()+10*GdxUtils.getDeltaTime());
		if(Gdx.input.isKeyPressed(Keys.H))
			moving.setX(moving.getX()-10*GdxUtils.getDeltaTime());
		if(Gdx.input.isKeyPressed(Keys.U))
			moving.setY(moving.getY()+10*GdxUtils.getDeltaTime());
		if(Gdx.input.isKeyPressed(Keys.J))
			moving.setY(moving.getY()-10*GdxUtils.getDeltaTime());
		
		camera.update();
		
		lights.beginNormal();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
			batch.draw(normalMap, 0, 0, 20, 20);
		batch.end();
		lights.endNormal();
		
		lights.beginOccludor();
		ShapeRenderer r = new ShapeRenderer();
		r.setProjectionMatrix(camera.combined);
		r.begin(ShapeType.Filled);
		r.setColor(1, 0, 0, 0.1f);
		r.box(10, 10, 0, 5, 1, 0);
		r.setColor(0, 0, 1, 0.1f);
		r.box(10, 12, 0, 5, 1, 0);
		r.end();
		lights.endOccludor();
		
		lights.begin();
		
		GdxUtils.clearScreen(Color.BLACK);
		
		batch.setProjectionMatrix(camera.combined);
		
		batch.begin();
			batch.draw(game, 0, 0, 20, 20);
		batch.end();
		
		lights.end();
		
		lights.finalizeRender(camera);
	}
}
