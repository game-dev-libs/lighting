package lights.test;

import java.util.function.Supplier;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.felixcool98.gdxutility.GdxUtils;
import com.felixcool98.lights.DirectionalLight;
import com.felixcool98.lights.LightSystem;
import com.felixcool98.lights.PointLight;
import com.felixcool98.lights.PointLightSquare;;

public class LightSystemTest extends ApplicationAdapter {
	public static void test(Supplier<LightSystem> lightSystem) {
		new Lwjgl3Application(new LightSystemTest(lightSystem), new Lwjgl3ApplicationConfiguration());
	}
	
	
	private Supplier<LightSystem> supplier;
	
	
	private Texture game;
	private Texture normalMap;
	
	private OrthographicCamera camera;
	
	private SpriteBatch batch;
	
	private LightSystem lights;
	
	private PointLight moving;
	
	
	public LightSystemTest(Supplier<LightSystem> lightSystem) {
		supplier = lightSystem;
	}
	
	
	@Override
	public void create() {
		game = new Texture(Gdx.files.local("test.png"));
		normalMap = new Texture(Gdx.files.local("test_normal.png"));
		
		camera = new OrthographicCamera(20, 20*GdxUtils.getAspectRatio());
		camera.position.set(camera.viewportWidth/2f, camera.viewportHeight/2f, 0);
		
		batch = new SpriteBatch();
		
		lights = supplier.get();
		
		moving = new PointLight(0, 0, 8, Color.WHITE);
		
		lights.add(new PointLight(0, 0, 3, Color.RED));
		lights.add(new PointLight(0, 0, 3, Color.BLUE));
		lights.add(new PointLight(0, 0, 3, Color.RED));
		lights.add(new PointLight(2, 7, 1, Color.GREEN));
		lights.add(new PointLight(10, 10, 4, Color.WHITE));
		lights.add(new PointLight(12, 10, 4, Color.WHITE));//14 10 4 WHITE
		lights.add(new PointLight(0, 0, 10, Color.RED));//0 0 WHITE
		lights.add(new PointLight(6, 0, 10, Color.BLUE));
		lights.add(new PointLight(20, 10, 10, Color.WHITE));
		
		lights.add(new PointLightSquare(3, 12, 1, 2, Color.GOLD));
		
		lights.add(new DirectionalLight(Color.FIREBRICK, 1, 1));
		
		lights.add(new PointLightSquare(3, 10, 1, 2, Color.CYAN));
		lights.add(new PointLightSquare(4, 10, 1, 2, Color.CYAN));
		lights.add(moving);
	}
	
	@Override
	public void render() {
		if(Gdx.input.isKeyPressed(Keys.W))
			camera.position.add(0, 0.1f, 0);
		if(Gdx.input.isKeyPressed(Keys.S))
			camera.position.add(0, -0.1f, 0);
		if(Gdx.input.isKeyPressed(Keys.A))
			camera.position.add(-0.1f, 0, 0);
		if(Gdx.input.isKeyPressed(Keys.D))
			camera.position.add(0.1f, 0, 0);
		
		if(Gdx.input.isKeyPressed(Keys.K))
			moving.setX(moving.getX()+10*GdxUtils.getDeltaTime());
		if(Gdx.input.isKeyPressed(Keys.H))
			moving.setX(moving.getX()-10*GdxUtils.getDeltaTime());
		if(Gdx.input.isKeyPressed(Keys.U))
			moving.setY(moving.getY()+10*GdxUtils.getDeltaTime());
		if(Gdx.input.isKeyPressed(Keys.J))
			moving.setY(moving.getY()-10*GdxUtils.getDeltaTime());
		
		camera.update();
		
		lights.beginNormal();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
			batch.draw(normalMap, 0, 0, 20, 20);
		batch.end();
		lights.endNormal();
		
		lights.begin();
		
		GdxUtils.clearScreen(Color.BLACK);
		
		batch.setProjectionMatrix(camera.combined);
		
		batch.begin();
			batch.draw(game, 0, 0, 20, 20);
		batch.end();
		
		lights.end();
		
		lights.finalizeRender(camera);
	}
}
