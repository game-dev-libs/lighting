package com.felixcool98.lights;

import com.badlogic.gdx.graphics.Color;

/**
 * an rectangular area fully lit by the given color 
 * 
 * @author felixcool98
 */
public class LigthArea implements Light {
	private Color color;
	private float x, y;
	private float width, height;
	
	
	public LigthArea(Color color, float x, float y, float width, float height) {
		this.color = color;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	
	public Color getColor() {
		return color;
	}
	
	public float getWidth() {
		return width;
	}
	public float getHeight() {
		return height;
	}
	
	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}
}
