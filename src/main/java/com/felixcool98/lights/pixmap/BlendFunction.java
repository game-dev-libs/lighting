package com.felixcool98.lights.pixmap;
import com.badlogic.gdx.graphics.Color;

public interface BlendFunction {
	public Color blend(Color src, Color dst);
	
	
	/**
	 * https://stackoverflow.com/questions/726549/algorithm-for-additive-color-mixing-for-rgb-values
	 */
	public static class DefaultBlending implements BlendFunction{
		@Override
		public Color blend(Color src, Color dst) {
			//DST_ALPHA SRC_ALPHA
			float a = 1 - (1 - dst.a) * (1 - src.a);
			float r = dst.r * dst.a / a + src.r * src.a *(1-dst.a) / a;
			float g = dst.g * dst.a / a + src.g * src.a *(1-dst.a) / a;
			float b = dst.b * dst.a / a + src.b * src.a *(1-dst.a) / a;
		
			return new Color(r, g, b, a);
		}
	}
}
