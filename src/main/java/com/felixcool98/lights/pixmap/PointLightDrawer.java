package com.felixcool98.lights.pixmap;
import com.badlogic.gdx.graphics.Color;
import com.felixcool98.lights.PointLight;
import com.felixcool98.math.VectorMath;

public class PointLightDrawer extends LightDrawer<PointLight> {
	public PointLightDrawer(LightMap lightMap, int pixelPerUnit) {
		super(lightMap, pixelPerUnit);
	}
	
	
	@Override
	protected Color getColor(float x, float y, PointLight light) {
		float factor = VectorMath.vectorLength(light.getX(), light.getY(), x, y)/light.getRadius();
		
		factor = factor > 1 ? 1 : factor;
		factor = 1-factor;
		
		Color color = light.getColor();
		
		return new Color(color.r, color.g, color.b, 0).lerp(color, factor);
	}
	
	@Override
	protected float getLeft(PointLight light) {
		return light.getX() - light.getRadius();
	}
	@Override
	protected float getBottom(PointLight light) {
		return light.getY() - light.getRadius();
	}
	@Override
	protected float getRight(PointLight light) {
		return light.getX() + light.getRadius();
	}
	@Override
	protected float getTop(PointLight light) {
		return light.getY() + light.getRadius();
	}
}
