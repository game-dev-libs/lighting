package com.felixcool98.lights.pixmap;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Blending;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.math.Vector2;

public class LightMap {
	private Pixmap pixmap;
	private Pixmap lightPositions;
	
	private BlendFunction blending = new BlendFunction.DefaultBlending();
	
	
	public LightMap(int width, int height) {
		this.pixmap = new Pixmap(width, height, Format.RGBA8888);
		lightPositions = new Pixmap(width, height, Format.RGBA8888);
	}
	
	
	public void begin() {
		pixmap.setBlending(Blending.None);
		pixmap.setColor(0, 0, 0, 0);
		pixmap.fill();
		pixmap.setBlending(Blending.SourceOver);
		
		lightPositions.setBlending(Blending.None);
		lightPositions.setColor(0, 0, 0, 0);
		lightPositions.fill();
		lightPositions.setBlending(Blending.SourceOver);
	}
	public void end() {
		for(int i = 0; i < pixmap.getWidth(); i++) {
			for(int j = 0; j < pixmap.getHeight(); j++) {
				Color color = new Color(pixmap.getPixel(i, j));
				
				if(color.a == 0){
					pixmap.setColor(Color.BLACK);
					pixmap.drawPixel(i, j);
				}else {
					float a = color.a;
					color.lerp(Color.BLACK, 1-color.a);
					
					color.a = color.a - a/2f;
					
					pixmap.setColor(color);
					pixmap.drawPixel(i, j);
				}
			}
		}
	}


	public void draw(int px, int py, Color dst, float lightX, float lightY) {
		if(dst.a == 0)
			return;
		
		lightPositions.setBlending(Blending.None);{
			Vector2 v = new Vector2(lightX, lightY);
			v.nor();
			
			Color posDst = new Color(v.x*0.5f + 0.5f, v.y*0.5f + 0.5f, 1, 1);
			
			lightPositions.setColor(posDst);
			lightPositions.drawPixel(px, py);
		}lightPositions.setBlending(Blending.SourceOver);
		
		pixmap.setBlending(Blending.None);{
			Color src = new Color(pixmap.getPixel(px, py));
			pixmap.setColor(blending.blend(src, dst));
			pixmap.drawPixel(px, py);
		}pixmap.setBlending(Blending.SourceOver);
	}
	
	
	public Pixmap getPixmap() {
		return pixmap;
	}
	public Pixmap getLightPositions() {
		return lightPositions;
	}
	
	public int getWidth() {
		return pixmap.getWidth();
	}
	public int getHeight() {
		return pixmap.getHeight();
	}
}
