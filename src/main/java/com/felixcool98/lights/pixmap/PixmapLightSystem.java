package com.felixcool98.lights.pixmap;
import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.felixcool98.lights.DirectionalLight;
import com.felixcool98.lights.LightSystem;
import com.felixcool98.lights.PointLight;
import com.felixcool98.lights.PointLightSquare;
import com.felixcool98.lights.utils.NormalMapper;

//TODO find definition for lights
//maybe getLeft getRight etc. to make drawing easier

public class PixmapLightSystem implements LightSystem {
	private List<PointLight> pointLights = new LinkedList<PointLight>();
	private List<PointLightSquare> pointLightSquares = new LinkedList<PointLightSquare>();
	
	
	private PointLightDrawer pointLightDrawer;
	private PointLightSquareDrawer pointLightSquareDrawer;
	
	private LightMap lightMap;
	
	private SpriteBatch batch;
	
	private OrthographicCamera camera;
	
	private ShaderProgram shader;
	
	private FrameBuffer fbo;
	
	private NormalMapper normalMapper;
	
	
	public PixmapLightSystem(int pixelPerUnit, int width, int height) {
		lightMap = new LightMap(width*pixelPerUnit, height*pixelPerUnit);
		
		pointLightDrawer = new PointLightDrawer(lightMap, pixelPerUnit);
		pointLightSquareDrawer = new PointLightSquareDrawer(lightMap, pixelPerUnit);
		
		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.setToOrtho(true);
		
		shader = new ShaderProgram(
				Gdx.files.classpath("com/felixcool98/lights/light.vert"),
				Gdx.files.classpath("com/felixcool98/lights/light.frag"));
		
		if(!shader.isCompiled())
			System.out.println(shader.getLog());
		if(shader.getLog().length() != 0)
			System.out.println(shader.getLog());
		
		
		fbo = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
		
		batch = new SpriteBatch();
		
		normalMapper = new NormalMapper();
	}
	
	
	private void renderLights(OrthographicCamera camera) {
		lightMap.begin();
		
		for(PointLight light : pointLights)
			pointLightDrawer.draw(light, camera);
		for(PointLightSquare light : pointLightSquares)
			pointLightSquareDrawer.draw(light, camera);
		
		lightMap.end();
	}
	
	public void beginNormal() {
		normalMapper.begin();
	}
	public void endNormal() {
		normalMapper.end();
	}
	
	@Override
	public void begin() {
		fbo.begin();
	}
	@Override
	public void end() {
		fbo.end();
	}
	
	@Override
	public void finalizeRender(OrthographicCamera c) {
		renderLights(c);
		
		Texture lightTexture = new Texture(lightMap.getPixmap());
		Texture lightPositionsTexture = new Texture(lightMap.getLightPositions());
		
		batch.setProjectionMatrix(camera.combined);
		
		batch.setShader(shader);
		
		shader.begin();{
			lightPositionsTexture.bind(3);
			shader.setUniformi("u_lightPos", 3);
			
			normalMapper.bind(2);
			shader.setUniformi("u_normal", 2);
			
			lightTexture.bind(1);
			shader.setUniformi("u_light_texture", 1);
			
			fbo.getColorBufferTexture().bind(0);
		}shader.end();
		
		batch.begin();
		batch.draw(fbo.getColorBufferTexture(), 0, 0);
//		batch.draw(lightPositionsTexture, 0, 0, camera.viewportWidth, camera.viewportHeight);
		batch.end();
	}
	
	@Override
	public void add(PointLight light) {
		pointLights.add(light);
	}
	@Override
	public void add(PointLightSquare light) {
		pointLightSquares.add(light);
	}
	@Override
	public void add(DirectionalLight light) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void clear() {
		pointLights.clear();
		pointLightSquares.clear();
	}
	
	
	public LightMap getLightMap() {
		return lightMap;
	}
}
