package com.felixcool98.lights.pixmap;
import com.badlogic.gdx.graphics.Color;
import com.felixcool98.lights.PointLightSquare;
import com.felixcool98.math.VectorMath;

public class PointLightSquareDrawer extends LightDrawer<PointLightSquare> {
	public PointLightSquareDrawer(LightMap lightMap, int pixelPerUnit) {
		super(lightMap, pixelPerUnit);
	}

	
	@Override
	public Color getColor(float x, float y, PointLightSquare light) {
		//closest point on square
		float nx = light.getX() > x ? light.getX()//left of square
				: light.getX() + light.getRadius() < x ? light.getX() + light.getRadius() ://right of square
				x;// in bounds
		float ny = light.getY() > y ? light.getY()//bellow square
				: light.getY() + light.getRadius() < y ? light.getY() + light.getRadius() ://above square
					y;//in bounds
		
		float distance = VectorMath.vectorLength(nx, ny, x, y);
		float factor;
		if(distance > light.getRadius()) {
			factor = 0;
		}else if(distance == 0) {
			factor = 1;
		}else {
			factor = distance / light.getRadius();
			
			factor = factor > 1 ? 1 : factor;
			factor = 1-factor;
		}
		
		Color color = light.getColor();
		
		return new Color(color.r, color.g, color.b, 0).lerp(color, factor);
	}
	
	@Override
	protected float getLeft(PointLightSquare light) {
		return light.getX() - light.getRadius();
	}
	@Override
	protected float getBottom(PointLightSquare light) {
		return light.getY() - light.getRadius();
	}
	@Override
	protected float getRight(PointLightSquare light) {
		return light.getX() + light.getRadius() + light.getSize()*2f;
	}
	@Override
	protected float getTop(PointLightSquare light) {
		return light.getY() + light.getRadius() + light.getSize()*2f;
	}
}
