package com.felixcool98.lights.pixmap;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;

public abstract class LightDrawer<T> {
	protected LightMap lightMap;
	protected int pixelPerUnit;
	
	
	public LightDrawer(LightMap lightMap, int pixelPerUnit) {
		this.lightMap = lightMap;
		this.pixelPerUnit = pixelPerUnit;
	}
	
	
	public void draw(T light, OrthographicCamera camera) {
		float screenLeft = getLeft(light) - camera.position.x + camera.viewportWidth/2f;
		float screenRight = getRight(light) - camera.position.x + camera.viewportWidth/2f;
		float screenBottom = getBottom(light) - camera.position.y + camera.viewportHeight/2f;
		float screenTop = getTop(light) - camera.position.y + camera.viewportHeight/2f;
		
		int lightSpaceLeft = (int) (screenLeft) * pixelPerUnit;
		int lightSpaceBottom = (int) (screenBottom) * pixelPerUnit;
		
		int lightSpaceRight = (int) Math.ceil((screenRight) * (float)pixelPerUnit);
		int lightSpaceTop = (int) Math.ceil((screenTop) * (float)pixelPerUnit);
		
		int startX = lightSpaceLeft < 0 ? 0 : lightSpaceLeft;
		int startY = lightSpaceBottom < 0 ? 0 : lightSpaceBottom;
		
		int endX = lightSpaceRight > lightMap.getWidth() ? lightMap.getWidth() : lightSpaceRight;
		int endY = lightSpaceTop > lightMap.getHeight() ? lightMap.getHeight() : lightSpaceTop;
		
		float width = getRight(light)-getLeft(light);
		float height = getTop(light)-getBottom(light);
		
		float lightX  = getLeft(light) + width/2f;
		float lightY  = getBottom(light) + height/2f;
		
		//use lightSpace borders to restrict the light to the current screen
		for(int px = startX; px < endX; px++) {
			for(int py = startY; py < endY; py++) {
				float x = toWorldCoordsX(px, camera);
				float y = toWorldCoordsY(py, camera);
				
				Color color = getColor(x, y, light);
				
				float lightDirX = lightX-x;
				float lightDirY = lightY-y;
				
				lightMap.draw(px, py, color, lightDirX, lightDirY);
			}
		}
	}
	
	
	private float toWorldCoordsX(int px, OrthographicCamera camera) {
		return camera.position.x - camera.viewportWidth/2f + (float)px/(float)pixelPerUnit;
	}
	private float toWorldCoordsY(int py, OrthographicCamera camera) {
		return camera.position.y - camera.viewportHeight/2f + (float)py/(float)pixelPerUnit;
	}
	
	protected abstract Color getColor(float x, float y, T light);
	
	/**
	 * @return leftest point the light can reach
	 */
	protected abstract float getLeft(T light);
	/**
	 * @return lowest point the light can reach
	 */
	protected abstract float getBottom(T light);
	/**
	 * @return rightest point the light can reach
	 */
	protected abstract float getRight(T light);
	/**
	 * @return highest point the light can reach
	 */
	protected abstract float getTop(T light);
}
