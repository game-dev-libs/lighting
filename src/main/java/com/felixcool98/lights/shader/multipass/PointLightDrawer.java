package com.felixcool98.lights.shader.multipass;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;

class PointLightDrawer {
	private FrameBuffer lightBuffer;
	
	
	PointLightDrawer(){
		this.lightBuffer = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
	}
	FrameBuffer getLightBuffer() {
		return lightBuffer;
	}
}
