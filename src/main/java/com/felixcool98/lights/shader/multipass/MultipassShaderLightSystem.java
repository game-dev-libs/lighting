package com.felixcool98.lights.shader.multipass;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.felixcool98.lights.DirectionalLight;
import com.felixcool98.lights.LightSystem;
import com.felixcool98.lights.PointLight;
import com.felixcool98.lights.PointLightSquare;
import com.felixcool98.lights.utils.NormalMapper;
import com.felixcool98.lights.utils.ShaderUtils;

public class MultipassShaderLightSystem implements LightSystem {
	private ShaderProgram pointLightShader;
	private ShaderProgram finalizeShader;
	
	private NormalMapper normalMapper;
	
	private FrameBuffer fbo;
	
	private PointLightDrawer pointLightDrawer;
	
	private FrameBuffer lightsBuffer;
	
	private ArrayList<PointLight> pointLights = new ArrayList<PointLight>();
	private ArrayList<PointLightSquare> pointLightSquares = new ArrayList<PointLightSquare>();
	private ArrayList<DirectionalLight> directionalLights = new ArrayList<DirectionalLight>();
	
	private Batch batch;
	
	private OrthographicCamera camera;
	
	
	public MultipassShaderLightSystem() {
		ShaderProgram.pedantic = false;
		
		pointLightShader = ShaderUtils.parseShader(
				Gdx.files.classpath("com/felixcool98/lights/shader/multipass"), "pointlight");
		finalizeShader = ShaderUtils.parseShader(
				Gdx.files.classpath("com/felixcool98/lights/shader/multipass"), "finalize");
		
		fbo = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
		
		lightsBuffer = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
		
		normalMapper = new NormalMapper();
		
		batch = new SpriteBatch();
		
		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.setToOrtho(true);
		
		pointLightDrawer = new PointLightDrawer();
	}
	

	@Override
	public void beginNormal() {
		normalMapper.begin();
	}
	@Override
	public void endNormal() {
		normalMapper.end();
	}
	

	@Override
	public void begin() {
		fbo.begin();
	}
	@Override
	public void end() {
		fbo.end();
	}
	
	@Override
	public void finalizeRender(OrthographicCamera c) {
		batch.setProjectionMatrix(camera.combined);
		
		lightsBuffer.begin();
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		Gdx.gl.glClearColor(0, 0, 0, 0);
		lightsBuffer.end();
		
		drawLights(c);
		
		finalizeShader.begin();{
			lightsBuffer.getColorBufferTexture().bind(1);
			finalizeShader.setUniformi("u_lights", 1);
			
			fbo.getColorBufferTexture().bind(0);
		}finalizeShader.end();
		
		batch.setShader(finalizeShader);
		
		batch.begin();{
			batch.draw(fbo.getColorBufferTexture(), 0, 0);
		}batch.end();
	}

	@Override
	public void add(PointLight light) {
		pointLights.add(light);
	}
	@Override
	public void add(PointLightSquare light) {
		pointLightSquares.add(light);
	}
	@Override
	public void add(DirectionalLight light) {
		directionalLights.add(light);
	}

	@Override
	public void clear() {
		pointLights.clear();
		pointLightSquares.clear();
		directionalLights.clear();
	}
	
	
	private void drawLights(OrthographicCamera c) {
		batch.setProjectionMatrix(camera.combined);
		
		pointLightShader.begin();
		
		pointLightShader.setUniformf("u_cameraPos", new Vector2(c.position.x, c.position.y));
		pointLightShader.setUniformf("u_cameraConv", new Vector2(c.viewportWidth/camera.viewportWidth, c.viewportHeight/camera.viewportHeight));
		pointLightShader.setUniformf("u_cameraSize", new Vector2(c.viewportWidth, c.viewportHeight));
		
		pointLightShader.end();
		
		for(PointLight light : pointLights) {
			drawLight(light);
		}
	}
	private void drawLight(PointLight light) {
		drawToLightBuffer(light);
		combineBuffers();
	}
	private void drawToLightBuffer(PointLight light) {
		pointLightShader.begin();{
			normalMapper.bind(1);
			pointLightShader.setUniformi("u_normal", 1);
			
			pointLightDrawer.getLightBuffer().getColorBufferTexture().bind(0);
			
			pointLightShader.setUniformf("u_light.position", new Vector2(light.getX(), light.getY()));
			pointLightShader.setUniformf("u_light.color", light.getColor());
			pointLightShader.setUniformf("u_light.range", light.getRadius());
		}pointLightShader.end();
		
		batch.setShader(pointLightShader);
		
		pointLightDrawer.getLightBuffer().begin();{
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
			Gdx.gl.glClearColor(0, 0, 0, 0);
			
			batch.begin();{
				batch.draw(pointLightDrawer.getLightBuffer().getColorBufferTexture(), 0, 0);
			}batch.end();
		}pointLightDrawer.getLightBuffer().end();
	}
	private void combineBuffers() {
		batch.enableBlending();
		batch.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE);
		
		lightsBuffer.begin();{
			batch.begin();{
				batch.draw(pointLightDrawer.getLightBuffer().getColorBufferTexture(), 0, 0);
			}batch.end();
		}lightsBuffer.end();
		
		batch.disableBlending();
	}

	
	@Override
	public String toString() {
		return "ShaderLightSystem "+pointLights.size()+" "+pointLightSquares.size();
	}
}
