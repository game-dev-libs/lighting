package com.felixcool98.lights.shader;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.felixcool98.lights.DirectionalLight;
import com.felixcool98.lights.LightSystem;
import com.felixcool98.lights.PointLight;
import com.felixcool98.lights.PointLightSquare;
import com.felixcool98.lights.utils.NormalMapper;
import com.felixcool98.lights.utils.ShaderUtils;

/**
 * simple gpu lighting<br>
 * 
 * [x] point lights<br>
 * [x] point light squares<br>
 * [x] normal maps<br>
 * [ ] shadows<br>
 * 
 * @author felixcool98
 */
public class ShaderLightSystem implements LightSystem {
	private ShaderProgram shader;
	
	private NormalMapper normalMapper;
	
	private FrameBuffer fbo;
	
	private ArrayList<PointLight> pointLights = new ArrayList<PointLight>();
	private ArrayList<PointLightSquare> pointLightSquares = new ArrayList<PointLightSquare>();
	private ArrayList<DirectionalLight> directionalLights = new ArrayList<DirectionalLight>();
	
	private Batch batch;
	
	private OrthographicCamera camera;
	
	
	public ShaderLightSystem() {
		shader = ShaderUtils.parseShader(Gdx.files.classpath("com/felixcool98/lights/shader"), "light");
		
		fbo = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
		
		normalMapper = new NormalMapper();
		
		batch = new SpriteBatch();
		
		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.setToOrtho(true);
	}
	

	@Override
	public void beginNormal() {
		normalMapper.begin();
	}
	@Override
	public void endNormal() {
		normalMapper.end();
	}
	

	@Override
	public void begin() {
		fbo.begin();
	}
	@Override
	public void end() {
		fbo.end();
	}
	
	@Override
	public void finalizeRender(OrthographicCamera c) {
		Texture fboTexture = fbo.getColorBufferTexture();
		
		batch.setProjectionMatrix(camera.combined);
		
		batch.setShader(shader);
		
		shader.begin();
		
		shader.setUniformf("u_cameraPos", new Vector2(c.position.x, c.position.y));
		shader.setUniformf("u_cameraConv", new Vector2(c.viewportWidth/camera.viewportWidth, c.viewportHeight/camera.viewportHeight));
		shader.setUniformf("u_cameraSize", new Vector2(c.viewportWidth, c.viewportHeight));
		
		addLights(pointLights.toArray(new PointLight[0]));
		addLights(pointLightSquares.toArray(new PointLightSquare[0]));
		addLights(directionalLights.toArray(new DirectionalLight[0]));
		
		normalMapper.bind(1);
		shader.setUniformi("u_normal", 1);
		
		fboTexture.bind(0);
		
		shader.end();
		
		batch.begin();
		batch.draw(fbo.getColorBufferTexture(), 0, 0);
		batch.end();
	}

	@Override
	public void add(PointLight light) {
		pointLights.add(light);
	}
	@Override
	public void add(PointLightSquare light) {
		pointLightSquares.add(light);
	}
	@Override
	public void add(DirectionalLight light) {
		directionalLights.add(light);
	}

	@Override
	public void clear() {
		pointLights.clear();
		pointLightSquares.clear();
		directionalLights.clear();
	}

	
	private void addLights(PointLight[] lights) {
		shader.setUniformi("u_pointLightAmount", lights.length);
		
		for(int i = 0; i < lights.length; i++) {
			PointLight light = lights[i];
			
			shader.setUniformf("u_pointLights["+i+"].position", new Vector2(light.getX(), light.getY()));
			shader.setUniformf("u_pointLights["+i+"].color", light.getColor());
			shader.setUniformf("u_pointLights["+i+"].range", light.getRadius());
		}
	}
	private void addLights(PointLightSquare[] lights) {
		shader.setUniformi("u_pointLightSquareAmount", Math.min(100, lights.length));
		
		for(int i = 0; i < lights.length && i < 100; i++) {
			PointLightSquare light = lights[i];
			
			shader.setUniformf("u_pointLightSquares["+i+"].position", new Vector2(light.getX(), light.getY()));
			shader.setUniformf("u_pointLightSquares["+i+"].size", light.getSize());
			shader.setUniformf("u_pointLightSquares["+i+"].color", light.getColor());
			shader.setUniformf("u_pointLightSquares["+i+"].range", light.getRadius());
		}
	}
	private void addLights(DirectionalLight[] lights) {
		shader.setUniformi("u_directionalLightAmount", Math.min(2, lights.length));
		
		for(int i = 0; i < lights.length && i < 2; i++) {
			DirectionalLight light = lights[i];
			
			shader.setUniformf("u_directionalLights["+i+"].direction", new Vector2(light.getDirX(), light.getDirY()));
			shader.setUniformf("u_directionalLights["+i+"].color", light.getColor());
		}
	}
	
	
	@Override
	public String toString() {
		return "ShaderLightSystem "+pointLights.size()+" "+pointLightSquares.size();
	}
}
