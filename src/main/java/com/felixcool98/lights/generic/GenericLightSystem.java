package com.felixcool98.lights.generic;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.felixcool98.lights.DirectionalLight;
import com.felixcool98.lights.LightSystem;
import com.felixcool98.lights.PointLight;
import com.felixcool98.lights.PointLightSquare;
import com.felixcool98.lights.utils.NormalMapper;
import com.felixcool98.lights.utils.OccluderMapper;
import com.felixcool98.lights.utils.ShaderUtils;

public class GenericLightSystem implements LightSystem {
	private LightDrawer lightDrawer;
	
	private NormalMapper normalMapper;
	private OccluderMapper occludorMapper;
	
	private FrameBuffer fbo;
	
	private ArrayList<PointLight> pointLights = new ArrayList<PointLight>();
	private ArrayList<DirectionalLight> directionalLights = new ArrayList<DirectionalLight>();
	
	private OrthographicCamera camera;
	
	private Batch batch;
	
	private ShaderProgram finalShader;
	
	
	public GenericLightSystem(LightDrawer lightDrawer) {
		this.lightDrawer = lightDrawer;
		
		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.setToOrtho(true);
		
		batch = new SpriteBatch();
		
		finalShader = ShaderUtils.parseShader(Gdx.files.classpath("com/felixcool98/lights/generic"), "final");
		
		fbo = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
		
		normalMapper = new NormalMapper();
		occludorMapper = new OccluderMapper();
	}
	
	
	public void setLightDrawer(LightDrawer lightDrawer) {
		this.lightDrawer = lightDrawer;
	}
	
	
	@Override
	public void beginNormal() {
		normalMapper.begin();
	}
	@Override
	public void endNormal() {
		normalMapper.end();
	}
	@Override
	public Texture getNormalView() {
		return normalMapper.texture();
	}
	
	@Override
	public boolean supportsOccludors() {
		return true;
	}
	public void beginOccludor() {
		occludorMapper.begin();
	}
	public void endOccludor() {
		occludorMapper.end();
	}
	@Override
	public Texture getOccludorView() {
		return occludorMapper.texture();
	}
	

	@Override
	public void begin() {
		fbo.begin();
	}
	@Override
	public void end() {
		fbo.end();
	}
	
	
	@Override
	public void finalizeRender(OrthographicCamera c) {
		lightDrawer.renderLights(
				occludorMapper.texture(), normalMapper.texture(),
				pointLights, directionalLights,
				c,
				new Vector2(additionalOcclusionSize()[0], additionalOcclusionSize()[1]));
		
		batch.setProjectionMatrix(camera.combined);
		
		batch.setShader(finalShader);
		
		finalShader.begin();{
			lightDrawer.bind(1);
			finalShader.setUniformi("u_lights", 1);
			
			fbo.getColorBufferTexture().bind(0);
		}finalShader.end();
		
		batch.begin();{
			batch.draw(fbo.getColorBufferTexture(), 0, 0);
		}batch.end();
	}
	@Override
	public Texture getLightView() {
		return lightDrawer.texture();
	}
	
	
	
	@Override
	public void add(PointLight light) {
		pointLights.add(light);
	}

	@Override
	public void add(PointLightSquare light) {
//		throw new UnsupportedOperationException();
	}
	@Override
	public void add(DirectionalLight light) {
		directionalLights.add(light);
	}

	@Override
	public void clear() {
		pointLights.clear();
	}
}
