package com.felixcool98.lights.generic;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.felixcool98.lights.DirectionalLight;
import com.felixcool98.lights.PointLight;

public interface LightDrawer {
	public void renderLights(Texture occludors, Texture normals,
			Iterable<PointLight> pointLights, Iterable<DirectionalLight> directionalLights, 
			OrthographicCamera c,
			Vector2 additionalOcclusionSize);
	
	public void bind(int id);
	
	public Texture texture();
}
