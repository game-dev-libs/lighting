package com.felixcool98.lights.generic.simple;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.felixcool98.gdxutility.GdxUtils;
import com.felixcool98.lights.DirectionalLight;
import com.felixcool98.lights.PointLight;
import com.felixcool98.lights.generic.LightDrawer;
import com.felixcool98.lights.utils.ShaderUtils;

public class SimpleLightDrawer implements LightDrawer {
	private Batch batch;
	private FrameBuffer lightBuffer;
	
	private OrthographicCamera camera;
	
	private ShaderProgram lightShader;
	
	
	public SimpleLightDrawer() {
		this(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}
	public SimpleLightDrawer(int width, int height) {
		lightShader = ShaderUtils.parseShader(Gdx.files.classpath("com/felixcool98/lights/generic/simple"), "light");
		
		lightBuffer =  new FrameBuffer(Format.RGBA8888, width, height, false);
		
		batch = new SpriteBatch();
		
		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.setToOrtho(true);
	}
	
	
	@Override
	public void renderLights(Texture occludors, Texture normals,
				Iterable<PointLight> pointLights, Iterable<DirectionalLight> directionalLights, 
				OrthographicCamera c,
				Vector2 additionalOcclusionSize) {
		batch.setProjectionMatrix(camera.combined);
		
		batch.setShader(lightShader);
		
		lightShader.begin();{
			lightShader.setUniformf("u_cameraPos", new Vector2(c.position.x, c.position.y));
			lightShader.setUniformf("u_cameraConv", new Vector2(c.viewportWidth/camera.viewportWidth, c.viewportHeight/camera.viewportHeight));
			lightShader.setUniformf("u_cameraSize", new Vector2(c.viewportWidth, c.viewportHeight));
			
			addPointLights(pointLights);
			addLights(directionalLights);
			
			normals.bind(0);
		}lightShader.end();
		
		lightBuffer.begin();{
			GdxUtils.clearScreen(1, 1, 1, 0f);
			
			batch.begin();{
				batch.draw(normals, 0, 0);
			}batch.end();
		}lightBuffer.end();
	}
	
	@Override
	public void bind(int id) {
		lightBuffer.getColorBufferTexture().bind(id);
	}
	@Override
	public Texture texture() {
		return lightBuffer.getColorBufferTexture();
	}
	
	
	private void addPointLights(Iterable<PointLight> lights) {
		int i = 0;
		
		for(PointLight light : lights) {
			lightShader.setUniformf("u_pointLights["+i+"].position", new Vector2(light.getX(), light.getY()));
			lightShader.setUniformf("u_pointLights["+i+"].color", light.getColor());
			lightShader.setUniformf("u_pointLights["+i+"].range", light.getRadius());
			
			i++;
		}
		
		lightShader.setUniformi("u_pointLightAmount", i);
	}
	private void addLights(Iterable<DirectionalLight> lights) {
		int i = 0;
		
		for(DirectionalLight light : lights) {
			lightShader.setUniformf("u_directionalLights["+i+"].direction", new Vector2(light.getDirX(), light.getDirY()));
			lightShader.setUniformf("u_directionalLights["+i+"].color", light.getColor());
			
			i++;
			
			if(i >= 2)
				break;
		}
		
		lightShader.setUniformi("u_directionalLightAmount", Math.min(2, i));
	}
}
