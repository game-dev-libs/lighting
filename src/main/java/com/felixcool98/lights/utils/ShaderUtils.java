package com.felixcool98.lights.utils;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

public class ShaderUtils {
	public static ShaderProgram parseShader(FileHandle dir, String name) {
		return parseShader(dir, name, "vert", "frag");
	}
	public static ShaderProgram parseShader(FileHandle dir, String name, String vertExt, String fragExt) {
		return parseShader(dir.child(name+"."+vertExt), dir.child(name+"."+fragExt));
	}
	public static ShaderProgram parseShader(FileHandle vert, FileHandle frag) {
		ShaderProgram program = new ShaderProgram(vert, frag);
		
		if(!program.isCompiled())
			System.err.println(program.getLog());
		else if(program.getLog().length() != 0)
			System.out.println(program.getLog());
			
		return program;
	}
}
