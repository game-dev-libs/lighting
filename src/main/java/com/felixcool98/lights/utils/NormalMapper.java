package com.felixcool98.lights.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.felixcool98.gdxutility.GdxUtils;

public class NormalMapper {
	private FrameBuffer fbo;
	
	
	public NormalMapper() {
		fbo = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
	}
	
	
	public void begin() {
		fbo.begin();
		
		GdxUtils.clearScreen(0.5f, 0.5f, 1, 1);
	}
	public void end() {
		fbo.end();
	}
	
	
	public void bind(int unit) {
		fbo.getColorBufferTexture().bind(unit);
	}
	public Texture texture() {
		return fbo.getColorBufferTexture();
	}
}
