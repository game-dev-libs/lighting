package com.felixcool98.lights.utils;

import com.felixcool98.collections.pools.ConcurrentPool;
import com.felixcool98.collections.pools.Pool;
import com.felixcool98.lights.DirectionalLight;
import com.felixcool98.lights.Light;

public class LightPools {
	private static Pool<DirectionalLight> directionalLights = new ConcurrentPool<DirectionalLight>(1) {
		@Override
		public DirectionalLight create() {
			return new DirectionalLight();
		}
	};
	
	
	public static DirectionalLight DirectionalLight() {
		return directionalLights.get();
	}
	
	
	public static void free(Light light) {
		if(light instanceof DirectionalLight) {
			((DirectionalLight) light).set(null, 0, 0);
			
			directionalLights.free((com.felixcool98.lights.DirectionalLight) light);
		}
	}
}
