package com.felixcool98.lights.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.felixcool98.gdxutility.GdxUtils;

/**
 * occludors block light<br>
 * the alpha defines the occlusion amount<br>
 * the color components the tint
 * 
 * @author felixcool98
 */
public class OccluderMapper {
	private FrameBuffer fbo;
	
	private float additionalWidth, additionalHeight;
	
	
	public OccluderMapper() {
		fbo = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
	}
	
	
	public void setAdditionalWidth(float additionalWidth) {
		this.additionalWidth = additionalWidth;
	}
	public void setAdditionalHeight(float additionalHeight) {
		this.additionalHeight = additionalHeight;
	}
	
	
	public void begin() {
		fbo.begin();
		
		GdxUtils.clearScreen(1, 1, 1, 0f);
	}
	public void end() {
		fbo.end();
	}
	
	
	public void bind(int unit) {
		fbo.getColorBufferTexture().bind(unit);
	}
	public Texture texture() {
		return fbo.getColorBufferTexture();
	}
	
	
	public float getAdditionalWidth() {
		return additionalWidth;
	}
	public float getAdditionalHeight() {
		return additionalHeight;
	}
}
