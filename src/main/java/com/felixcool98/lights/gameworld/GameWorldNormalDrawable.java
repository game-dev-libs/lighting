package com.felixcool98.lights.gameworld;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.felixcool98.gameworld.GameWorldDrawable;

public interface GameWorldNormalDrawable {
	public void drawNormal(Batch batch);
	
	/**
	 * if true it will call {@link GameWorldDrawable#draw(Batch)} once with a special shader to create a normal backround<br>
	 * can be used to fix bugs with rotated or offset drawings<br>
	 * should be set to false
	 */
	public default boolean safeNormal() {
		return false;
	}
}
