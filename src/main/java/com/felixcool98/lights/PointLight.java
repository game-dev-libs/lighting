package com.felixcool98.lights;

import com.badlogic.gdx.graphics.Color;

/**
 * a light with a radius and a color
 * 
 * @author felix
 */
public class PointLight implements Light {
	private float x, y;
	private float radius;
	private Color color;
	
	
	public PointLight(float x, float y, float radius, Color color) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.color = color;
	}
	
	
	public void setX(float x) {
		this.x = x;
	}
	public void setY(float y) {
		this.y = y;
	}
	
	public void setColor(Color color) {
		this.color = color;
	}
	
	public void setRadius(float radius) {
		this.radius = radius;
	}
	
	
	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}
	
	public float getRadius() {
		return radius;
	}
	
	
	public Color getColor() {
		return color;
	}
}
