package com.felixcool98.lights;
import com.badlogic.gdx.graphics.Color;

/**
 * a light fully illuminating a square
 * 
 * @author felix
 */
public class PointLightSquare implements Light {
	private float x, y;
	private float size;
	private float radius;
	private Color color;
	
	
	public PointLightSquare(float x, float y, float size, float radius, Color color) {
		this.x = x;
		this.y = y;
		
		this.size = size;
		this.radius = radius;
		this.color = color;
	}


	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}

	public float getSize() {
		return size;
	}

	public float getRadius() {
		return radius;
	}

	public Color getColor() {
		return color;
	}
}
