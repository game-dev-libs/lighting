package com.felixcool98.lights;

import java.util.List;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.felixcool98.gameworld.GameWorld;

public interface GameWorldLightSystem {
	public void render(OrthographicCamera camera, GameWorld world, List<Light> globals);
}
