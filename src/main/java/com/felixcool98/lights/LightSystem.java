package com.felixcool98.lights;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;

/**
 * Lightsystems are meant to be an easy way to add lights to any game<br>
 * not all lighsystems support all features so the documentation should have a list of all included features similar to this:<br>
 * 
 * [x] point lights<br>
 * [x] point light squares<br>
 * [ ] normal maps<br>
 * [ ] shadows<br>
 * 
 * @author felixcool98
 */
public interface LightSystem {
	public default boolean supportsOccludors() {return false;}
	
	public default float[] additionalOcclusionSize() {return new float[] {14, 14};};
	
	public default void beginOccludor() {}
	public default void endOccludor() {}
	
	/**
	 * begins drawing normal maps
	 */
	public void beginNormal();
	/**
	 * ends drawing normal maps
	 */
	public void endNormal();
	
	/**
	 * must be called before rendering the scene
	 */
	public void begin();
	/**
	 * must be called after rendering the scene
	 */
	public void end();
	
	/**
	 * must be called after all rendering is done<br>
	 * draws the lighted scene
	 * 
	 * @param camera the camera that draws the scene
	 */
	public void finalizeRender(OrthographicCamera camera);
	
	
	public default void add(Light light) {
		if(light instanceof PointLight)
			add((PointLight) light);
		if(light instanceof PointLightSquare)
			add((PointLightSquare) light);
		if(light instanceof DirectionalLight)
			add((DirectionalLight) light);
	}
	public void add(PointLight light);
	public void add(PointLightSquare light);
	public void add(DirectionalLight light);
	
	public void clear();
	
	
	public default Texture getNormalView() {
		return null;
	}
	public default Texture getOccludorView() {
		return null;
	}
	public default Texture getLightView() {
		return null;
	}
}
