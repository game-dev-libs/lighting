package com.felixcool98.lights;

import com.badlogic.gdx.graphics.Color;

public class DirectionalLight implements Light {
	private Color color;
	private float dirX, dirY;
	
	
	public DirectionalLight() {
		
	}
	public DirectionalLight(Color color, float dirX, float dirY) {
		set(color, dirX, dirY);
	}
	
	
	public DirectionalLight set(Color color, float dirX, float dirY) {
		this.color = color;
		
		this.dirX = dirX;
		this.dirY = dirY;
		
		return this;
	}


	public Color getColor() {
		return color;
	}


	public float getDirX() {
		return dirX;
	}
	public float getDirY() {
		return dirY;
	}
}
