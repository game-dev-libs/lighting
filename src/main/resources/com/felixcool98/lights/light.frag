#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif
varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform sampler2D u_light_texture;
uniform sampler2D u_normal;
uniform sampler2D u_lightPos;

void main()
{
	vec4 normal = texture2D(u_normal, v_texCoords);
	vec4 lightColor = texture2D(u_light_texture, v_texCoords);
	vec4 light = texture2D(u_lightPos, v_texCoords);
	vec4 texture = texture2D(u_texture, v_texCoords);

	vec3 LightDir = vec3(light.rgb);

	vec3 N = normalize(normal.rgb * 2.0 - 1.0);
	vec3 L = normalize(LightDir);

	vec3 Diffuse = (lightColor.rgb * lightColor.a) * max(dot(N, L), 0.0);
	vec3 Intensity = Diffuse;
	vec3 FinalColor = texture.rgb * Intensity;

	gl_FragColor = v_color * vec4(FinalColor, texture.a);
//	gl_FragColor = vec4(vec3(dot(N, L)), 1);
//	gl_FragColor = vec4(N, 1);
}
