#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform sampler2D u_normal;

varying vec2 fragPos;

//point lights
struct PointLight{
	vec2 position;
	vec4 color;
	float range;
};

uniform PointLight u_light;

vec3 calculatePointLight(PointLight light, vec2 fragPos, vec4 normal){
	vec3 LightDir = vec3(light.position - fragPos, 1);

	vec3 N = normalize(normal.rgb * 2.0 - 1.0);
	vec3 L = normalize(LightDir);

	float diff = max(dot(N, L), 0.0);

	float dist = length(fragPos - light.position);
	float attentuation = 1.0 - dist/light.range;

	if(dist > light.range)
		attentuation = 0.0;

	vec4 lightColor = light.color;
	vec3 Diffuse = (light.color.rgb * light.color.a) * diff * attentuation;

	return Diffuse;
}

void main(){
	vec4 normal = texture2D(u_normal, v_texCoords);
	vec4 texture = texture2D(u_texture, v_texCoords);

	vec3 outp = calculatePointLight(u_light, fragPos, normal);

	vec4 lightColor = vec4(outp, 1);

	gl_FragColor = lightColor;
}
