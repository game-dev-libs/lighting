attribute vec4 a_position;
attribute vec4 a_color;
attribute vec2 a_texCoord0;
uniform mat4 u_projTrans;
varying vec4 v_color;
varying vec2 v_texCoords;

uniform vec2 u_cameraPos;
uniform vec2 u_cameraSize;
uniform vec2 u_cameraConv;

varying vec2 fragPos;

void main(){
	fragPos = vec2(a_position.x*u_cameraConv.x, u_cameraSize.y-a_position.y*u_cameraConv.y) + (u_cameraPos - u_cameraSize/2.0);

   v_color = a_color;
   v_color.a = v_color.a * (255.0/254.0);
   v_texCoords = a_texCoord0;
   gl_Position =  u_projTrans * a_position;
}
