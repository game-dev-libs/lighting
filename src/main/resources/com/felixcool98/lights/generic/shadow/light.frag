#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif
in LOWP vec4 v_color;
in vec2 v_texCoords;

uniform float u_occlusionStepSize;

uniform vec2 u_cameraPos;
uniform vec2 u_cameraSize;

uniform sampler2D u_texture;
uniform sampler2D u_occludors;

uniform vec2 u_additionalSize;

in vec2 fragPos;

//point lights
struct PointLight{
	vec2 position;
	vec4 color;
	float range;
};

uniform int u_pointLightAmount;
uniform PointLight u_pointLights[200];

//directional lights
struct DirectionalLight{
	vec2 direction;
	vec4 color;
};

uniform int u_directionalLightAmount;
uniform DirectionalLight u_directionalLights[2];

vec2 bottomLeft(){
	return u_cameraPos - u_cameraSize/2.0 - u_additionalSize;
}

vec4 occlusionAtPoint(vec2 point){
	vec2 bottomLeft = bottomLeft() + vec2(u_additionalSize/2.0);

	vec2 coords = (point - bottomLeft) / (u_cameraSize + u_additionalSize);

	return texture2D(u_occludors, coords);
}

bool outOfBounds(vec2 point){
	vec2 pos = vec2(point);

	vec2 bottomLeft = bottomLeft();
	vec2 topRight = u_cameraPos + u_cameraSize/2.0 + u_additionalSize;

	return any(lessThan(pos, bottomLeft)) || any(greaterThan(pos, topRight));
}

struct RayData{
	vec3 occlusionColor;
	float occlusion;
	bool unreachable;
};

RayData rayTrace(vec2 start, vec2 dir, float max, float stepSize){
	dir = normalize(dir);

	float occlusion = 0.0;
	vec3 occlusionColor = vec3(1.0);

	for(float i = 0.0; i < max; i += stepSize){
		vec2 point = start + dir * i;

		if(outOfBounds(point))
			return RayData(occlusionColor, occlusion, false);

		vec4 occ = occlusionAtPoint(point);

		occlusion += occ.a*stepSize;

		if(occlusion > 1.0)//skip if fully occluded
			return RayData(vec3(1.0), 1.0, true);

		occlusionColor *= occ.rgb;
	}

	return RayData(occlusionColor, occlusion, false);
}

vec3 calculatePointLight(PointLight light, vec2 fragPos, vec4 normal){
	float dist = length(fragPos - light.position);

	if(dist > light.range)//skip if out of range
		return vec3(0.0);

	vec3 LightDir = vec3(light.position - fragPos, 1);

	float stepSize = u_occlusionStepSize;

	RayData rayData = rayTrace(fragPos, LightDir.xy, dist, stepSize);

	if(rayData.unreachable)
		return vec3(0.0);

	vec3 occlusionColor = rayData.occlusionColor;
	float occlusionAmount = rayData.occlusion;

	if(occlusionAmount > 1.0)
		occlusionAmount = 1.0;

	occlusionAmount = 1.0 - occlusionAmount;

	vec4 occlusion = vec4(vec3(1.0), occlusionAmount);

	vec3 N = normalize(normal.rgb * 2.0 - 1.0);
	vec3 L = normalize(LightDir);

	float diff = max(dot(N, L), 0.0);

	float attentuation = 1.0 - dist/light.range;

	vec4 lightColor = light.color;
	vec3 Diffuse = (light.color.rgb * light.color.a) * diff * attentuation * occlusionAmount * occlusionColor;

	return Diffuse;
}

vec3 calculateDirectionalLight(DirectionalLight light, vec2 fragPos, vec4 normal){
	vec3 LightDir = vec3(light.direction, 1);

	float stepSize = 0.1;

	RayData rayData = rayTrace(fragPos, LightDir.xy, length(u_cameraSize), stepSize);

	if(rayData.unreachable)
		return vec3(0.0);

	vec3 occlusionColor = rayData.occlusionColor;
	float occlusionAmount = rayData.occlusion;

	if(occlusionAmount > 1.0)
		occlusionAmount = 1.0;

	occlusionAmount = 1.0 - occlusionAmount;

	vec3 N = normalize(normal.rgb * 2.0 - 1.0);
	vec3 L = normalize(LightDir);

	float diff = max(dot(N, L), 0.0);

	vec4 lightColor = light.color;
	vec3 Diffuse = (light.color.rgb * light.color.a) * diff * occlusionAmount * occlusionColor;

	return Diffuse;
}

void main(){
	vec4 normal = texture2D(u_texture, v_texCoords);

	vec3 outp = vec3(0.0);

	for(int i = 0; i < u_pointLightAmount; i++)
		outp += calculatePointLight(u_pointLights[i], fragPos, normal);
	for(int i = 0; i < u_directionalLightAmount; i++)
		outp += calculateDirectionalLight(u_directionalLights[i], fragPos, normal);

	vec4 lightColor = vec4(outp, 1);

	gl_FragColor = lightColor * v_color;
}
