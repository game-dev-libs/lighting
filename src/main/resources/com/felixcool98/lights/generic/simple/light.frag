#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif
varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;

varying vec2 fragPos;

//point lights
struct PointLight{
	vec2 position;
	vec4 color;
	float range;
};

uniform int u_pointLightAmount;
uniform PointLight u_pointLights[130];

//point light square
struct PointLightSquare{
	vec2 position;
	float size;
	vec4 color;
	float range;
};

uniform int u_pointLightSquareAmount;
uniform PointLightSquare u_pointLightSquares[100];

//directional light
struct DirectionalLight{
	vec2 direction;
	vec4 color;
};

uniform int u_directionalLightAmount;
uniform DirectionalLight u_directionalLights[2];

vec3 calculatePointLight(PointLight light, vec2 fragPos, vec4 normal){
	vec3 LightDir = vec3(light.position - fragPos, 1);

	vec3 N = normalize(normal.rgb * 2.0 - 1.0);
	vec3 L = normalize(LightDir);

	float diff = max(dot(N, L), 0.0);

	float dist = length(fragPos - light.position);
	float attentuation = 1.0 - dist/light.range;

	if(dist > light.range)
		attentuation = 0.0;

	vec4 lightColor = light.color;
	vec3 Diffuse = (light.color.rgb * light.color.a) * diff * attentuation;

	return Diffuse;
}
vec3 calculatePointLightSquares(PointLightSquare light, vec2 fragPos, vec4 normal){
	vec3 LightDir = vec3(light.position - fragPos, 1);

	vec3 N = normalize(normal.rgb * 2.0 - 1.0);
	vec3 L = normalize(LightDir);

	float diff = max(dot(N, L), 0.0);

	vec2 nearestLightPos = light.position;

	if(fragPos.x > light.position.x)
		nearestLightPos.x += light.size;
	if(fragPos.y > light.position.y)
		nearestLightPos.y += light.size;
	if(fragPos.x > light.position.x && fragPos.x < light.position.x + light.size)
		nearestLightPos.x = fragPos.x;
	if(fragPos.y > light.position.y && fragPos.y < light.position.y + light.size)
		nearestLightPos.y = fragPos.y;

	float dist = length(fragPos - nearestLightPos);
	float attentuation = 1.0 - dist/light.range;

	if(dist > light.range)
		attentuation = 0.0;

	vec4 lightColor = light.color;
	vec3 Diffuse = (light.color.rgb * light.color.a) * diff * attentuation;

	return Diffuse;
}

vec3 calculateDirectionalLight(DirectionalLight light, vec2 fragPos, vec4 normal){
	vec3 LightDir = vec3(light.direction, 1);

	vec3 N = normalize(normal.rgb * 2.0 - 1.0);
	vec3 L = normalize(LightDir);

	float diff = max(dot(N, L), 0.0);

	vec4 lightColor = light.color;
	vec3 Diffuse = (light.color.rgb * light.color.a) * diff;

	return Diffuse;
}

void main(){
	vec4 normal = texture2D(u_texture, v_texCoords);

	vec3 outp = vec3(0.0);

	for(int i = 0; i < u_pointLightAmount; i++)
		outp += calculatePointLight(u_pointLights[i], fragPos, normal);
	for(int i = 0; i < u_pointLightSquareAmount; i++)
		outp += calculatePointLightSquares(u_pointLightSquares[i], fragPos, normal);
	for(int i = 0; i < u_directionalLightAmount; i++)
		outp += calculateDirectionalLight(u_directionalLights[i], fragPos, normal);

	vec4 lightColor = vec4(outp, 1);

	gl_FragColor = lightColor * v_color;
}
