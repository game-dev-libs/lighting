#ifdef GL_ES
#define LOWP lowp
precision mediump float;
#else
#define LOWP
#endif
in LOWP vec4 v_color;
in vec2 v_texCoords;

uniform sampler2D u_texture;
uniform sampler2D u_lights;

void main(){
	vec4 lightColor = texture2D(u_lights, v_texCoords);
	vec4 texture = texture2D(u_texture, v_texCoords);

	gl_FragColor = lightColor * v_color * texture;
}
