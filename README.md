Provides a lightsystem for 2d games that is independent from any collision detection.  
Only 3 steps are required to render things:  

1. add lights to lightsystem
2. render normal maps of all objects
3. render all objects
4. finalize rendering

Different lightsystems will give different results ranging from primitive lighting to raytracing.