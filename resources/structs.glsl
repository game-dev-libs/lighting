struct PointLight{
	vec2 position;
	vec4 color;
	float range;
};

struct PointLightSquare{
	vec2 position;
	float size;
	vec4 color;
	float range;
};

struct DirectionalLight{
	vec2 direction;
	vec4 color;
};
